import React from 'react';

import { useForm } from "react-hook-form";

export default function Search(){

    const {register, handleSubmit, reset, errors} = useForm();

    const onSubmit = () => {
        //props.filterAmiibos(data);
        console.log();
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
        <input type="text" name="name" className="input" ref={register({require:true})}/>
        </form>
        )
};