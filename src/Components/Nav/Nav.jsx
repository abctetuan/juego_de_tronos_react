import React from 'react';
import { Link } from 'react-router-dom';
import './nav.scss';


export default function Nav(){
    return (
        <nav className="App-footer col-12">
            <div className="col-4">
                <Link to="/Home" className="link col-4">HOME</Link>
            </div>           
            <div className="col-4">
                <Link to="/Houses" className="link col-4">HOUSES</Link>
            </div>   
            <div className="col-4">
                <Link to="/Chronology" className="link col-4">CHRONOLOGY</Link>
            </div>   
        </nav>

    )
};
  