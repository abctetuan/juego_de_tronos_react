import React from 'react';
import './App.css';

import HomePages from './pages/HomePages/HomePages';
import CharactersPages from './pages/CharactersPages/CharactersPages';
import HousesPages from './pages/HousesPages/HausesPages';
import ChronologyPage from './pages/ChronologyPage/ChronologyPage';

import Header from './Components/Header/Header';
import Nav from './Components/Nav/Nav';


import {BrowserRouter as  Router, Route, Switch } from 'react-router-dom';


function App() {
  return (
    <div className="Home">
      <Router>
      <div className="App main">
        <Switch>
          <Route path="/Home"><HomePages /></Route>
          <Route path="/Characters"><CharactersPages /></Route>
          <Route path="/Houses"><HousesPages /></Route>
          <Route path="/Chronology"><ChronologyPage /></Route>
        </Switch>
      </div>
      <footer>
        <Nav />
      </footer>
    </Router>
    </div>
    
  );
}

export default App;
